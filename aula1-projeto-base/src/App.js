import React from 'react';
import './App.css';

function App() {

  
  return (
    <div> 

      //Header
      <div>

        <img 
        src={`${process.env.PUBLIC_URL}/logo.png`} 
        alt="Logomarca da garagem Fuscão Preto!" 
        />

        <a href="#"> Comprar </a>
        <a href="#"> Vender </a>

      </div>

      //Slider
      <div>

      <img 
        src={`${process.env.PUBLIC_URL}/slider/corsel.jpeg`} 
        alt="Logomarca da garagem Fuscão Preto!" 
        />
        
      <img 
        src={`${process.env.PUBLIC_URL}/slider/monza-tubarao.jpeg`} 
        alt="Logomarca da garagem Fuscão Preto!" 
        />

      </div>

      //Search
      <div>

        <form>

          <label>Marca</label>
          <select name="cars">
            <option value="volvo">Volvo</option>
            <option value="saab">Saab</option>
            <option value="fiat">Fiat</option>
            <option value="audi">Audi</option>
          </select>

          <label>Modelo</label>
          <select name="models">
            <option value="volvo">Civic</option>
            <option value="saab">City</option>
            <option value="fiat">HRV</option>
            <option value="audi">Marea Turbo</option>
          </select>

          <label>Ano</label>
          <select name="years">
            <option value="volvo">1999</option>
            <option value="saab">2000</option>
            <option value="fiat">2001</option>
            <option value="audi">2002</option>
          </select>

          <button type="submit"> Buscar </button>
          
        </form>

      </div>

      //Categories
      <div>

        <a href="#suv">
        <img 
        src={`${process.env.PUBLIC_URL}/categories/suvs.jfif`} 
        alt="Logomarca da garagem Fuscão Preto!" 
        />
        </a>

        <a href="#eletrico">
        <img 
        src={`${process.env.PUBLIC_URL}/categories/carro-eletrico.jfif`} 
        alt="Logomarca da garagem Fuscão Preto!" 
        />
        </a>

        <a href="#mini-van">
        <img 
        src={`${process.env.PUBLIC_URL}/categories/download.jfif`} 
        alt="Logomarca da garagem Fuscão Preto!" 
        />
        </a>



      </div>

      //Footer
      <div>

        <h2>Anúncio</h2>
        <a href="#comprar">Comprar</a>  
        <a href="#comprar">Vender</a>  

        <h2>Contato</h2>
        <a href="#comprar">Fale Conosco</a>  
        <a href="#comprar">Como vender</a> 
        <a href="#comprar">Como comprar</a> 

      </div>

    </div>
    
  );
}

export default App;
