import React from 'react';
import {Provider} from "react-redux";
import {BrowserRouter as Router } from 'react-router-dom';

import Routes from './Routes';
import AppHeader from './components/AppHeader.js';
import store from './store';

import './App.css';
import {Link} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

function App(){
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Routes/>
          </div>
        </Router>
      </Provider>
    )
  
  
}

export default App;