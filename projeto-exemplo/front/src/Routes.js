import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

// LAYOUT
import AuthLayout from './layout/AuthLayout';
import DashboardLayout from './layout/DashboardLayout';

// VIEWS
import NotFound from './components/NotFound';
import Dashboard from './views/dashboard/Dashboard'
import Login from './views/auth/Login'
import CarroLista from './views/dashboard/CarroLista'
import CarroForm from './views/dashboard/CarroForm'


const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
    <Route {...rest} render={props => (
      <Layout>
        <Component {...props} />
      </Layout>
    )} />
  )

export class Routes extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <AppRoute exact path="/" 
                      layout={AuthLayout} 
                      component={Login} />
                    <AppRoute exact path="/dashboard" 
                      layout={DashboardLayout} 
                      component={Dashboard} />
                    <AppRoute exact path="/carro" 
                      layout={DashboardLayout} 
                      component={CarroLista} />
                    <AppRoute exact path="/carro/novo" 
                      layout={DashboardLayout} 
                      component={CarroForm} />
                    <AppRoute exact path="/carro/novo/:id" 
                      layout={DashboardLayout} 
                      component={CarroForm} />
                </Switch>
            </div>
        )
    }
};

export default Routes;