import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Link, useHistory } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage, withFormik } from 'formik';

import { equipmentSelector } from "../../store/selectors/equipmentSelector";
import { saveEquipment } from '../../store/actions/equipmentActions';


const initialValues = {
    name: '',
    description: '',
    price: ''
};

function _handleSubmit(values, {setSubmitting}, props){
    console.log(values);

    console.log("PROPS");
    console.log(props);

    props.saveEquipment(values);
    setSubmitting(false);   

    props.history.goBack();
}

const EquipmentForm = (props) => (

    <Formik
        initialValues = { props.equipment || initialValues}
        onSubmit = { (values, actions) =>  _handleSubmit(values, actions, props)}        
    >
        

        {({values,  handleSubmit, isSubmitting}) => (

             <Form onSubmit={handleSubmit}>

                

             <div>
                 <label>Nome:</label>
                 <Field type="text" name="name" />
                 <ErrorMessage name="name" component="div" />
             </div>
 
             <div>
                 <label>Descrição:</label>
                 <Field type="text" name="description" />
                 <ErrorMessage name="description" component="div" />
             </div>
 
             <div>
                 <label>Preço:</label>
                 <Field type="number" name="price" />
                 <ErrorMessage name="price" component="div" />
             </div>
 
             <div>
                 <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Save</button>
                 <Link to="/equipment" className="btn btn-danger">Cancel</Link>
             </div>
 

         </Form>
        )}

    </Formik>

);


export default connect(equipmentSelector, { saveEquipment })(EquipmentForm);









/*
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Link, useHistory } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage, withFormik } from 'formik';

import { equipmentSelector } from "../../store/selectors/equipmentSelector";
import { saveEquipment } from '../../store/actions/equipmentActions';


const initialValues = {
    name: '',
    description: '',
    price: ''
};

function _handleSubmit(values, {setSubmitting}, saveEquipment){
    console.log(values);
    saveEquipment(values);
    setSubmitting(false);   
}

function _handleSubmit2(props){
    console.log('_handleSubmit2');
    console.log(props);
 
}

const EquipmentForm = ({equipment, saveEquipment }) => (

    <Formik
        initialValues = { equipment || initialValues}
        onSubmit = { (values, actions) =>  _handleSubmit(values, actions, saveEquipment)}        
    >

        {({isSubmitting}) => (

             <Form>
             <div>
                 <label>Nome:</label>
                 <Field type="text" name="name" />
                 <ErrorMessage name="name" component="div" />
             </div>
 
             <div>
                 <label>Descrição:</label>
                 <Field type="text" name="description" />
                 <ErrorMessage name="description" component="div" />
             </div>
 
             <div>
                 <label>Preço:</label>
                 <Field type="number" name="price" />
                 <ErrorMessage name="price" component="div" />
             </div>
 
             <div>
                 <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Save</button>
                 <Link to="/equipment" className="btn btn-danger">Cancel</Link>
             </div>
 

         </Form>
        )}

    </Formik>

);


export default connect(equipmentSelector, { saveEquipment })(EquipmentForm);
*/