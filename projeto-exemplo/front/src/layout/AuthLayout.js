import React, {Component} from 'react';

export class AuthLayout extends Component {
    render() {
        return (
            <div>
                <h1>AuthLayout</h1>
                {this.props.children}
            </div>
        )
   }

};

export default AuthLayout;