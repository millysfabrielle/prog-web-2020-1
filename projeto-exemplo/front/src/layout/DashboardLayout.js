import React, { Component } from 'react';
import {Link} from "react-router-dom";
import './DashboardLayout.css';

export class DashboardLayout extends Component {
    render() {
        return (
            <div>

                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#">Navbar</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav">
                            <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                            <a class="nav-item nav-link" href="#">Features</a>
                            <a class="nav-item nav-link" href="#">Pricing</a>
                            <a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                        </div>
                    </div>
                </nav>

                <div className="row">
                    <div className='sidebarMenu col-sm-2'>
                        <ul>
                            <li> <Link to="/carro" >Carro LINK</Link> </li>
                            <li> <a href="#">Link 1</a></li>
                        </ul>
                    </div>
                    <div className='content col-sm-10'>
                        {this.props.children}
                    </div>
                </div>

                <div className="row">
                    <div className="footer col">
                        Footer
                    </div>
                </div>

                
            </div>
        )
    }

};

export default DashboardLayout;