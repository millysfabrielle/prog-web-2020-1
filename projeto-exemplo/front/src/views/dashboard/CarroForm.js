import React, {Component} from 'react';
import {Link, useParams} from "react-router-dom";

export default class CarroForm extends Component {

    componentDidMount() {
        
        console.log(this.props.match.params);

    }
    
    render() {
        return (
            <div>
                <h1>Cadastro de carro</h1>

                <form>

                    <div className="form-group">
                        <label for="carroPlaca">Placa</label>
                        <input type="text" className="form-control" id="carroPlaca" placeholder="AAA-9999"></input>
                    </div>
                    <div className="form-group">
                        <label for="carroMarca">Marca</label>
                        <select id="carroMarca" class="form-control form-control-lg">
                            <option>Chevrolet</option>
                            <option>Honda</option>
                            <option>Toyota</option>
                        </select>
                    </div>

                    <button type="submit" >Salvar</button>
                    <Link to="/carro">Voltar</Link>

                </form>

            </div>
        )
   }

};