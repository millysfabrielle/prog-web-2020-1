import React, {Component} from 'react';
import {Link} from "react-router-dom";

const id = 2;

export class CarroLista extends Component {
    
    render() {

        // JSX
        return (
            <div>

                <h1>Lista de carros</h1>

                <Link to='/carro/novo' >Novo</Link>

                <table className="table">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Modelo</th>
                            <th>Placa</th>
                            <th>Ano</th>
                            <th>Operações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Honda</td>
                            <td>PQQ-0980</td>
                            <td>2016</td>
                            <td>
                                {/* INTERPOLAÇÃO é feita com CRASE */}
                                <Link to={`/carro/novo/${id}`}>Alterar</Link>
                                <button> Deletar </button>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        )
   }

};

export default CarroLista;